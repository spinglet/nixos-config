let
  krops = (import <nixpkgs> {}).fetchgit {
    url = "https://github.com/poppysingleton/krops";
    rev = "sean/test-tty";
    sha256 = "sha256-4uget0Q2orTg5kyTGDjZMQMUkb8rkcYUQx65bhHu7Us=";
  };

  lib = import "${krops}/lib";
  pkgs = import "${krops}/pkgs" {};

  importJson =  (import <nixpkgs> {}).lib.importJSON;
  source = {name, jsonfile}: lib.evalSource [{
    nixpkgs.git = {
      shallow = true; # fixes fatal: fetch-pack: invalid index-pack output
      clean.exclude = ["/.version-suffix"]; # todo work out why this is excluded!
      # updatde with: nix-shell -p nix-prefetch-git --command 'nix-prefetch-git   --url https://github.com/NixOS/nixpkgs   --rev refs/heads/nixos-21.11-aarch64   > nixos-21.11-aarch64.json'
      ref = (importJson jsonfile).rev;
      url = (importJson jsonfile).url;
    };
    config.file = toString ./config;
    my-devices.file = toString ./my-devices;
    nixos-config.symlink = "config/configuration.nix";
    "imports.nix".symlink = "my-devices/${name}.nix";

    #    above results in this tree on the device
    #├── config
    #│     ├── configuration.nix
    #│     ├── machine-specific
    #│     └── roles
    #├── imports.nix -> my-devices/gitlab-runner-4.nix
    #├── my-devices
    #│     ├── boat-anyliser.nix
    #│     └── gitlab-runner-4.nix
    #├── nixos-config -> config/configuration.nix

  }];

  snork = pkgs.krops.writeDeploy "snork" {
    source = source {name ="snork"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/snork.nix}";
    crossDeploy = true;
    force = true;
  };

  boat-hermes = pkgs.krops.writeDeploy "boat-hermes" {
    source = source {name = "boat-hermes"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/boat-hermes.nix}";
    crossDeploy = true;
  };

  temp-cm4 = pkgs.krops.writeDeploy "temp-cm4" {
      source = source {name = "temp-cm4"; jsonfile = ./nixos-21.11-aarch64.json;};
      target = "krops@${import ./config/vars/ips/temp-cm4.nix}";
      crossDeploy = true;
    };

  boat-athena = pkgs.krops.writeDeploy "boat-athena" {
    source = source {name = "boat-athena"; jsonfile = ./nixos-aarch64.json;};
    target = "krops@${import ./config/vars/ips/boat-athena.nix}";
    crossDeploy = true;
  };
  # force = true; # auto add sentinal stuff
  gitlab-runner-6 = pkgs.krops.writeDeploy "gitlab-runner-6" {
    source = source {name = "gitlab-runner-6"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gitlab-runner-6.nix}";
    crossDeploy = true;
  };

  gitlab-runner-5 = pkgs.krops.writeDeploy "gitlab-runner-5" {
    source = source {name = "gitlab-runner-5"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gitlab-runner-5.nix}";
    crossDeploy = true;
    force = true; # auto add sentinal stuff
  };

  gitlab-runner-4 = pkgs.krops.writeDeploy "gitlab-runner-4" {
    source = source {name = "gitlab-runner-4"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gitlab-runner-4.nix}";
    crossDeploy = true;
  };

  gitlab-runner-3 = pkgs.krops.writeDeploy "gitlab-runner-3" {
    source = source {name = "gitlab-runner-3"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gitlab-runner-3.nix}";
    crossDeploy = true;
  };

  check-gitlab-runner-3 = pkgs.krops.writeDeploy "check-gitlab-runner-3" {
    source = source {name = "gitlab-runner-3"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gitlab-runner-3.nix}/home/krops/src-check";
    crossDeploy = true;
    command = targetPath: ''
      copy-nixpkgs.pl /home/krops/src-check
      nix-shell -p jq --command 'check-system.pl /home/krops/src-check'
    '';
  };

  gitlab-runner-2 = pkgs.krops.writeDeploy "gitlab-runner-2" {
    source = source {name = "gitlab-runner-2"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gitlab-runner-2.nix}";
    crossDeploy = true;
  };

  check-gitlab-runner-2 = pkgs.krops.writeDeploy "check-gitlab-runner-2" {
    source = source {name = "gitlab-runner-2"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gitlab-runner-2.nix}/home/krops/src-check";
    crossDeploy = true;
    command = targetPath: ''
      copy-nixpkgs.pl /home/krops/src-check
      nix-shell -p jq --command 'check-system.pl /home/krops/src-check'
    '';
  };

  gitlab-runner-1 = pkgs.krops.writeDeploy "gitlab-runner-1" {
    source = source {name = "gitlab-runner-1"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gitlab-runner-1.nix}";
    crossDeploy = true;
  };

  check-gitlab-runner-1 = pkgs.krops.writeDeploy "check-gitlab-runner-1" {
    source = source {name = "gitlab-runner-1"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gitlab-runner-1.nix}/home/krops/src-check";
    crossDeploy = true;
    command = targetPath: ''
      copy-nixpkgs.pl /home/krops/src-check
      nix-shell -p jq --command 'check-system.pl /home/krops/src-check'
    '';
  };

  nas = pkgs.krops.writeDeploy "nas" {
    source = source { name = "nas"; jsonfile = ./nixos-21.11.json;};
    target = "krops@${import ./config/vars/ips/nas.nix}";
  };

  check-nas = pkgs.krops.writeCommand "check-nas" {
    source = source { name = "nas"; jsonfile = ./nixos-21.11.json;};
    target = "krops@${import ./config/vars/ips/nas.nix}/home/krops/src-check";
    command = targetPath: ''
      copy-nixpkgs.pl /home/krops/src-check
      nix-shell -p jq --command 'check-system.pl /home/krops/src-check'
    '';
  };

  lenovo-laptop = pkgs.krops.writeDeploy "lenovo-laptop" {
    source = source { name = "lenovo-laptop"; jsonfile = ./nixos-x86.json;};
    target = "krops@${import ./config/vars/ips/lenovo-laptop.nix}";
  };

  check-lenovo-laptop = pkgs.krops.writeCommand "check-lenovo-laptop" {
    source = source { name = "lenovo-laptop"; jsonfile = ./nixos-x86.json;};
    target = "krops@${import ./config/vars/ips/lenovo-laptop.nix}/home/krops/src-check";
    command = targetPath: ''
      copy-nixpkgs.pl /home/krops/src-check
      nix-shell -p jq --command 'check-system.pl /home/krops/src-check'
    '';
  };

  gizmo = pkgs.krops.writeDeploy "gizmo" {
    source = source {name = "gizmo"; jsonfile = ./nixos-21.11-aarch64.json;};
    target = "krops@${import ./config/vars/ips/gizmo.nix}";
    crossDeploy = true;
  };

  rustah = pkgs.krops.writeDeploy "rustah" {
      source = source {name = "rustah"; jsonfile = ./nixos-21.11-aarch64.json;};
      target = "krops@${import ./config/vars/ips/rustah.nix}";
      crossDeploy = true;
    };
in {
  inherit snork;
  inherit temp-cm4;
  inherit boat-hermes;
  inherit boat-athena;
  inherit gitlab-runner-6;
  inherit gitlab-runner-5;
  inherit gitlab-runner-4;
  inherit gitlab-runner-3;
  inherit gitlab-runner-2;
  inherit gitlab-runner-1;
  inherit nas;
  inherit check-nas;
  inherit lenovo-laptop;
  inherit check-lenovo-laptop;
  inherit gizmo;
  inherit rustah;
  #  gitlab-runner-3 = gitlab-runner-3;
  all = pkgs.writeScript "deploy-all-servers"
    (lib.concatStringsSep "\n" [ boat-athena gitlab-runner-4 boat-hermes nas ]);
  boat = pkgs.writeScript "deploy-all-boat-computers"
    (lib.concatStringsSep "\n" [ boat-athena boat-hermes ]);
  gitlab-runners = pkgs.writeScript "deploy-all-gitlab-runners"
      (lib.concatStringsSep "\n" [ gitlab-runner-1 gitlab-runner-2 gitlab-runner-3 gitlab-runner-4 gitlab-runner-5 gitlab-runner-6]);
  deploy-new-gitlab-runners = pkgs.writeScript "deploy-new-gitlab-runners"
      (lib.concatStringsSep "\n" [ gitlab-runner-5 gitlab-runner-6]);
}

# nix-build ./krops.nix -A boat && ./result; rm result

# krops requires git :/

# login, copy initiall config, `nixos-rebuild switch`
# [root@nixos:~]# mkdir /var/src
# [root@nixos:~]# chown krops /var/src/
# [root@nixos:~]# passwd krops
# do you need any sops? if so go do the sops stuff or update withtout sops and do sops later
# run krops
# ssh back in and setup tailscale

# add git and tailscale to make life easier



## RPI 4 setup
# Download built image
# determin ip address, currently manualy done (should automate email to myself)
# login to root (yubikey needed) or krops
# [root@nixos:~]# passwd krops
# [krops@nixos:~]# passwd
# update krops password
# add /var/src and make it krops writeable
# [root@nixos:~]# mkdir /var/src
# [root@nixos:~]# chown krops /var/src/
# update krops
# updae sops if needed
# run krops
