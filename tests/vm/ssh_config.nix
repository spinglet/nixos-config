# Some tests to ensure build_ssh_config is working properly.
{pkgs}:
let
	insecurePubKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDPZkkHOpMezDkhTaDuIuuC1htPU+lFpo2DI9+XK/h9CF7kPlQ+xT+N6gsm51mYZ0qaCsU0qLKWDU2/IP0/ovIDLhoQDCrjv3eSuwbsMghXHZUquTcv0u62LMIsdjE5CRNXMtEZ+SJxTcDyUhAUNFnyOi2OvL/RpgJWAxEzkOb5MwMl1w+lihpI1fETfBGDrtXoa8PSMkbuHt75QetPwhG2j5TDF2L4EmEy9RVmNiT2rCMq5NOuWXuylOBXKGMIoPKyrLSdi66n2dlAmNGugNWFGnARPCsD9Wpnn7HH7IRYDhbM1DTI3TMPp2vKjs2zTX7Ydu5zlj2syTNAvG5fPslo3PnQ5440QEwKnw5/8D2JatLzXsJm0udrd6AXTG/PCFSKRmE6adQf+q0PHbAPy0CGjENDDL2T4iab9ePFfPnuZ+2u1s387iNm0cUB7rV3kKSxCb62utR4X6fdU8xkC10BVWgtpu2/KiqQGq2lVbMDwZ9PcQzMEYOO2SH/CruGYVE= sean@lenovo-laptop";
	insecurePrivateKey = ''-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEAz2ZJBzqTHsw5IU2g7iLrgtYbT1PpRaaNgyPflyv4fQhe5D5UPsU/
jeoLJudZmGdKmgrFNKiylg1NvyD9P6LyAy4aEAwq4793krsG7DIIVx2VKrk3L9LutizCLH
YxOQkTVzLRGfkicU3A8lIQFDRZ8jotjry/0aYCVgMRM5Dm+TMDJdcPpYoaSNXxE3wRg67V
6GvD0jJG7h7e+UHrT8IRto+Uwxdi+BJhMvUVZjYk9qwjKuTTrll7spTgVyhjCKDysqy0nY
uup9nZQJjRroDVhRpwETwrA/VqZ5+xx+yEWA4WzNQ0yN0zD6dryo7Ns01+2Hbuc5Y9rMkz
QLxuXz7JaNz50OeONEBMCp8Of/A9iWrS817CZtLna3egF0xvzwhUikZhOmnUH/qtDx2wD8
tAhoxDQwy9k+Imm/XjxXz57mftrtbN/O4jZtHFAe61d5CksQm+trrUeF+n3VPMZAtdAVVo
LabtvyoqkBqtpVWzA8GfT3EMzBGDjtkh/wq7hmFRAAAFiH6RGV1+kRldAAAAB3NzaC1yc2
EAAAGBAM9mSQc6kx7MOSFNoO4i64LWG09T6UWmjYMj35cr+H0IXuQ+VD7FP43qCybnWZhn
SpoKxTSospYNTb8g/T+i8gMuGhAMKuO/d5K7BuwyCFcdlSq5Ny/S7rYswix2MTkJE1cy0R
n5InFNwPJSEBQ0WfI6LY68v9GmAlYDETOQ5vkzAyXXD6WKGkjV8RN8EYOu1ehrw9IyRu4e
3vlB60/CEbaPlMMXYvgSYTL1FWY2JPasIyrk065Ze7KU4FcoYwig8rKstJ2LrqfZ2UCY0a
6A1YUacBE8KwP1amefscfshFgOFszUNMjdMw+na8qOzbNNfth27nOWPazJM0C8bl8+yWjc
+dDnjjRATAqfDn/wPYlq0vNewmbS52t3oBdMb88IVIpGYTpp1B/6rQ8dsA/LQIaMQ0MMvZ
PiJpv148V8+e5n7a7WzfzuI2bRxQHutXeQpLEJvra61Hhfp91TzGQLXQFVaC2m7b8qKpAa
raVVswPBn09xDMwRg47ZIf8Ku4ZhUQAAAAMBAAEAAAGAG8DyUNdTzLFDi+NZf8SIg4+nQ+
xUsQ9irIPmbdJihEjUefKlT2kR4inJqEEZSj24S/jY/vz8OPw6xTLi8NsbS7WlozQdoTCE
DcydVoozd4Tr6zWx3uJ5M0O2EEEYwSsnvWl9dd5S75qaEVNN94Yx6ypB4nH7sVBny57JdH
8ViY98AlpPK7XZYpnOku7tYRPKaDnTkJNNzpuu9ugNVzT/LdD09U5x3gSHoUwOuwjP1Xqf
kiPg99ndpb4qbz9mUUdINVH0dEcgJEqrTiIIRYKuty5tV8IyrF1kcDG8JKBSk8UtqxMCrK
H+aw1Ypfdgrl9tqBSnca9f+wKDKpBeNI+lSk4zPA6HMttUH/rYwBrLInQCtTPwTLznsg+f
cStJy9b4DE3Xqizhmmy/LRiJIx6IxX1/CHmP9EoVwZR3NdwvIkb+IDcCzRG0/+mN6KPGyr
lozIHfccwZN3tiRYlKQfVXtO0n5SbYdzgpX03D8Hk6OMOog1eNOxJOMjgXwDkb0pzhAAAA
wEWVk5uBP8bUTLEt0DpFVAofU4pB8UEW2pw+kP6CMS5pPUiVLZ6XPWTR64qplqG4KmOThq
HtCQmJolZ/OrLCHQV9kBI6kfPWJ615VOwvXGt+ahwEglG5gRsBxcEwGEIjVepZmUDJKSYA
Agap18pnaq2n13XvT758xI4RBEPG7Ah2koWSnqJbnuY9v7CaPKMmx3ETp+pG0XL7TRXLwS
+aVUR6ByoaDISynpsnfjnQR0a1T3LMk511/C2Vz8HNaPX3+QAAAMEA7Y2SFsPmaYfSPDjx
upvd+a/jiaQ+y8ZM2szD/zfVWKgcAdQyWCbxZBFUwCPJRXCZbfITwLS1YiCy6lKj/0Q5V+
ucq4qFmnLc1jl+6C/9fOkt+EEMzLj7phcNsGn/BrfWopbJYszK1mwtzvjN9RpCx4Zo4jYh
iH2F6VICdgWteBu8l82qCF2UXOyF3QhGNsbONt45nKvWrkHz89l3xd4qpJyK/IbjrAwNRT
I/WFyeuJYQF7nYxKSXL8OsykUjMxRlAAAAwQDfgUeXbmONsdeWo0r6Z7vEPuxTw8EPQdUN
m2YYrIiUkYxUO8yBvVkqzhYCiS5pihx7ymZV+SDFazJAPfuBJLfTMNZWSyHlzIevuiIoyE
VESPssi7aiEHNci44ADfboopQcaDG7zuZlVfAMBLVlBRATucR8flM/d7eI1T9LR79mh/ZD
5btklSs38Rpqu5mBldWanDJE3XRuf7s9u/TlLeg17pva0Q+tZl9lNFkZ+PE0/Sry0wjAJ/
BEgxkSazha/H0AAAASc2VhbkBsZW5vdm8tbGFwdG9wAQ==
-----END OPENSSH PRIVATE KEY-----
'';
in
 pkgs.nixosTest ({ pkgs, ...} : {
    name = "ssh_config";
    meta = with pkgs.lib.maintainers; {
      maintainers = [ seanborg ];
    };

    nodes.machine =
      {
        users.groups = { foobar = {}; barfoo = {}; baz = { gid = 1337; }; };
        users.users = {
          test0 = {
          	isNormalUser = true;
          	openssh.authorizedKeys.keys = [ "${insecurePubKey}"];
        	};
      	};
      	systemd.network.enable = true;
				environment.etc."insecurePrivateKey_rsa".text = insecurePrivateKey;
				services.openssh.enable = true;
				services.openssh.passwordAuthentication = false;
				programs.ssh.extraConfig = "${import ../../config/vars/ssh_config.nix}";
			};


    testScript =
      ''
        with subtest("Test config is correct"):
            machine.succeed('sleep 10 && su - test0 -c "ssh -vv -i /etc/insecurePrivateKey_rsa -o StrictHostKeyChecking=no test0@::1 echo test"')
      '';
  })