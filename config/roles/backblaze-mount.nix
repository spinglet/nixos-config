{ pkgs, lib, config, ... }:
let
    cyphermount = "/mnt/cypher";
    plainmount = "/mnt/plain";
    bucket = "mounted-bucket";

in
{
  imports = [ # Include the results of the hardware scan.
    ./sops.nix
  ];
  sops.secrets.b2_password = {
    sopsFile = ./backblaze-mount-secrets.yaml;
  };
  sops.secrets.gocrypt_password = {
    sopsFile = ./backblaze-mount-secrets.yaml;
  };

  systemd.services."s3fs-${bucket}" = {
    description = "Backblaze b2 storage S3FS";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStartPre = [
        "${pkgs.coreutils}/bin/mkdir -m 0500 -pv ${cyphermount}"
        "${pkgs.e2fsprogs}/bin/chattr +i ${cyphermount}"  # Stop files being accidentally written to unmounted directory
      ];
      ExecStart = let
        options = [
          "passwd_file=${ toString config.sops.secrets.b2_password.path }"
          "use_path_request_style"
          "allow_other"
          "url=https://s3.us-west-001.backblazeb2.com"  # Linode object storage
        ];
      in
        "${pkgs.s3fs}/bin/s3fs ${bucket} ${cyphermount} -f "
          + lib.concatMapStringsSep " " (opt: "-o ${opt}") options;
      ExecStopPost = "-${pkgs.fuse}/bin/fusermount -u ${cyphermount}";
      KillMode = "process";
      Restart = "on-failure";
    };
  };

  systemd.services."gocryptfs-${bucket}" = {
    description = "gocryptfs mount ${cyphermount}";
    wantedBy = [ "multi-user.target" ];
    after = [ "s3fs-${bucket}.service" ];
    requires = [ "s3fs-${bucket}.service" ];
    serviceConfig = {
      ExecStartPre = [
        "${pkgs.coreutils}/bin/mkdir -m 0500 -pv ${plainmount}"
        "${pkgs.e2fsprogs}/bin/chattr +i ${plainmount}"  # Stop files being accidentally written to unmounted directory
      ];
      ExecStart = "${pkgs.gocryptfs}/bin/gocryptfs ${cyphermount} ${plainmount} -fg -noprealloc -sharedstorage -passfile ${ toString config.sops.secrets.gocrypt_password.path} -allow_other";
      ExecStopPost = "-${pkgs.fuse}/bin/fusermount -u ${plainmount}";
      KillMode = "process";
      Restart = "on-failure";
    };
  };
}