{ config, pkgs, ... }:
{
  imports = [
    "${builtins.fetchTarball {
      url = "https://github.com/Mic92/sops-nix/archive/0e0dcc74bae23c7ef7fb6251c43c277b827e8c34.tar.gz";
      sha256 = "0s2gkg2sqi5qs3b9540hvs1vf8qa4746kw40nnmp2nzp25pc9ql5";
    }}/modules/sops"
  ];
}

# on system
# nix-shell -p ssh-to-pgp --run "ssh-to-pgp -i /etc/ssh/ssh_host_rsa_key -o server01.asc"
