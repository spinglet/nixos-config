let
  rclonePubSshKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBBYnqAOBc3i4DENGunWPILJAPxZjTi81WcKIZgZx0s4 sean@lenovo-laptop";
in{
  users.users.datasync = {
    isNormalUser = true;
    description = "User used to backup data";
    openssh.authorizedKeys.keys = [ "${rclonePubSshKey}" ];
  };
}