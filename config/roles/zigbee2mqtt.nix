{ pkgs, lib, config, ... }:
let
  composefile = "zigbee-docker-compose.yaml";
in
{
  virtualisation.docker.enable = true;
  networking.firewall.allowedTCPPorts = [ 8080 1883 ];
  environment.etc = {
    ${composefile} = {
      text = ''
         version: '3.8'
         services:
           mqtt:
             image: eclipse-mosquitto:2.0
             restart: unless-stopped
             volumes:
               - "/root/zigbee/mosquitto-data:/mosquitto"
             ports:
               - "1883:1883"
               - "9001:9001"
             command: "mosquitto -c /mosquitto-no-auth.conf"

           zigbee2mqtt:
             container_name: zigbee2mqtt
             restart: unless-stopped
             image: koenkk/zigbee2mqtt
             volumes:
               - /root/zigbee/zigbee2mqtt-data:/app/data
               - /run/udev:/run/udev:ro
             ports:
               - 8080:8080
             environment:
               - TZ=Europe/UK
             devices:
               - /dev/ttyACM0:/dev/ttyACM0
     '';
    };
  };

  systemd.services."zigbee2mqtt" = {
    description = "mqtt and zigbee2mqtt";
    wantedBy = [ "multi-user.target" ];
    after = [ "docker.service" "docker.socket" ];
    requires = [ "docker.service" "docker.socket" ];
    serviceConfig = {
      ExecStart = "${pkgs.docker-compose}/bin/docker-compose -f /etc/${composefile} up";
    };
    preStop = "${pkgs.docker-compose}/bin/docker-compose -f /etc/${composefile} down";
    reload = "${pkgs.docker-compose}/bin/docker-compose -f /etc/${composefile} restart";
  };

  imports = [
    ./backup-user.nix
  ];
  # due to running by docker the group doesnt naturally exist so I have to make it
  users.groups."zigbee".members = [ "datasync" ];
  # users.users.datasync = {
    # due to docker requires smome manual futzing of the directory to make this group
    # extraGroups = [ "zigbee" ];
  # };

}
