{
  boot.blacklistedKernelModules = [ "snd_pcsp" ]; # todo move to something else
  boot.extraModprobeConfig = ''
    options snd slots=snd-hda-intel
  '';
  services.tlp = {
    enable = true;
    settings = {
      CPU_SCALING_GOVERNOR_ON_BAT="powersave";
      CPU_SCALING_GOVERNOR_ON_AC="ondemand";

      # disable turbo boost on battery
      CPU_BOOST_ON_AC=1;
      CPU_BOOST_ON_BAT=0;

      # The following prevents the battery from charging fully to
      # preserve lifetime. Run `tlp fullcharge` to temporarily force
      # full charge.
      # https://linrunner.de/tlp/faq/battery.html#how-to-choose-good-battery-charge-thresholds
      START_CHARGE_THRESH_BAT0=60;
      STOP_CHARGE_THRESH_BAT0=80;
    };
  };
}
