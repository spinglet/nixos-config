{ config, pkgs, ... }:
let
  myXmodmap = pkgs.writeText "xkb-layout" ''
    keycode 107 = Super_R NoSymbol Super_R
  '';
  home-manager = builtins.fetchGit {
    url = "https://github.com/nix-community/home-manager";
    rev = "2860d7e3bb350f18f7477858f3513f9798896831";
      ref = "release-21.11";
  };
  git-prompt = builtins.fetchurl {
    url = "https://raw.github.com/git/git/master/contrib/completion/git-prompt.sh";
    sha256 = "03k2lsr8r94v9ksp0z40rk65g56nj7hpdf7z53w19v25wczyvg0z";
  };
in
{
  users.users.sean = {
    isNormalUser = true;
    home = "/home/sean";
    description = "myself";
    extraGroups = [ "wheel" "networkmanager" "scanner" "lp" ];
    openssh.authorizedKeys.keys = [ "${import ../../vars/ssh-public-keys/yubikey.nix}"];
  };
  services.udev.packages = with pkgs; [
    yubikey-personalization
  ];
  programs = {
    ssh.startAgent = false;

    ssh.extraConfig = "${import ../../vars/ssh_config.nix}";
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };
  services.pcscd.enable = true; # For yubikey manager https://github.com/NixOS/nixpkgs/issues/51362

  virtualisation.docker.enable = true;
  imports = [ (import "${home-manager}/nixos") ];

  home-manager.users.sean = {
    programs.git = {
      enable = true;
      ignores = [ "*.idea" "venv/"];
      extraConfig = {
        core = { editor = "vim"; };
        help = {autocorrect = "25";};
      };
    };
    programs.bash = {
      enable = true;
      bashrcExtra = '''';
      historyControl = ["erasedups"];
      historyIgnore = ["exit" "'history -w'"];
      initExtra = ''
        source ${git-prompt}
        export GIT_PS1_SHOWDIRTYSTATE=1
        export GIT_PS1_SHOWUPSTREAM="auto"
        export PS1='[\u@\h \W \T] $(__git_ps1 "(%s) ")\$ '
      '';
    };
  };
  #----=[ Fonts ]=----#
  fonts = {
    enableDefaultFonts = true;
    fonts = with pkgs; [ 
      pkgs.ubuntu_font_family
    ];
    fontconfig = {
      defaultFonts = {
        serif = [ "Ubuntu" ];
        sansSerif = [ "Ubuntu" ];
        monospace = [ "Ubuntu Mono" ];
      };
    };
  };

  services.printing = { enable = true; };
  services.printing.drivers = [ pkgs.samsung-unified-linux-driver pkgs.splix ];
  hardware.sane.enable = true;
#  users.users.sean.extraGroups = [ "scanner" "lp" ];
  
  environment.etc = {
    "testAutoStart" = {
      text = "Mod=Mod4";
      mode = "0777";
    };
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  # require startx
  #services.xserver.displayManager.startx.enable = true;
  services.xserver.desktopManager.mate.enable = true;
  services.xserver.windowManager.i3.enable = true;
  services.xserver.windowManager.i3.configFile = pkgs.writeText "i3config" "${import ./i3-config.nix}";
  

  # Configure keymap in X11
  services.xserver.layout = "gb";
  # services.xserver.xkbOptions = "eurosign:e";

  services.xserver.displayManager.sessionCommands = "${pkgs.xorg.xmodmap}/bin/xmodmap ${myXmodmap}";

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  hardware.bluetooth.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;


  # Todo make something that sets this automaticly
  # (c) For `nix-env`, `nix-build`, `nix-shell` or any other Nix command you can add
  #       { allowUnfree = true; }
  #     to ~/.config/nixpkgs/config.nix.
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  nixpkgs.overlays = [ (self: super: { local = import ./my-pkgs { pkgs = super; };})];
  environment.systemPackages = with pkgs; [
    firefox
    qt5.qttools
    xorg.xmodmap # needed or above command, I wonder if I can not add it here
    pkgs.jdk
    pkgs.jetbrains.idea-community 
    direnv
    nix-direnv
    xclip
    obsidian
    wpa_supplicant_gui
    mate.mate-terminal
    pkgs.local.scripts
    ## the bellow should go into my bash scripts packages
    xdotool
    jq
  ];

  environment.pathsToLink = [
    "/share/nix-direnv"
  ];

  qt5.enable = true;
  qt5.style = "adwaita-dark";
  qt5.platformTheme =  "gnome";

}


