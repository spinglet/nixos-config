''
  # i3 config file (v4)
  set $mod Mod4
  bindsym Mod1+F4 kill
  bindsym $mod+f fullscreen toggle
  bindsym $mod+Return exec mate-terminal
  bindsym $mod+Shift+r restart
  bindsym $mod+l exec i3lock
  exec_always polybar-msg cmd quit; polybar the_bar&disown

  set $mode_launcher Launcher
  bindsym $mod+o mode "$mode_launcher"

  mode "$mode_launcher" {
    bindsym f exec firefox
    bindsym i exec quasselclient
    bindsym j exec idea-community
    bindsym n exec nm-gui
    bindsym o exec obsidian
    bindsym t exec thunderbird # for now this only works on work machines need a better system
    bindsym Escape mode "default"
  }

  # Move focused window:
  bindsym $mod+Left move left
  bindsym $mod+Down move down
  bindsym $mod+Up move up
  bindsym $mod+Right move right

  # Change focused window:
  bindsym $mod+Shift+Left focus left
  bindsym $mod+Shift+Down focus down
  bindsym $mod+Shift+Up focus up
  bindsym $mod+Shift+Right focus right

  # change container layout (stacked, tabbed, toggle split)
  bindsym $mod+s layout stacking
  bindsym $mod+w layout tabbed
  bindsym $mod+e layout toggle split

  # Move around workspaces
  bindsym Mod1+Ctrl+Right exec NextWorkspace.pl
  bindsym Mod1+Mod4++Ctrl+Right exec nextIncrementWorkspace.pl
  bindsym Ctrl+Mod1+Shift+Right exec MoveNextWorkspaces.pl
  bindsym Ctrl+Mod1+Mod4+Shift+Right exec MoveIncrementWorkspaces.pl
  bindsym Ctrl+Mod1+Left exec PrevWorkspace.pl
  bindsym Ctrl+Mod1+Shift+Left exec MovePrevWorkspaces.pl

  bindsym F9 exec terminal.pl


  bindsym $mod+1 workspace 1
  bar {
    status_command i3status
  }

  focus_follows_mouse no
''