{
  security.pam.yubico = {
    enable = true;
    #debug = true;
    #control = "required"; # set this to require a password aswell TODO setup for work laptop
    mode = "challenge-response";
  };

    home-manager.users.sean = {
        programs.git = {
          userEmail = "sean@seanborg.tech";
          userName = "Sean Borg";
        };
    };
}