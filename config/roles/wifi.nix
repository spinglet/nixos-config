{ config, pkgs, ... }:
{
  imports = [
    ./sops.nix
  ];
  sops.secrets.networking-env-file = {
    sopsFile = ./wifi-secrets.yaml;
  };

  networking.wireless.environmentFile = config.sops.secrets.networking-env-file.path;
  networking.wireless.enable = true;
  networking.wireless.userControlled.enable = true;
  networking.wireless.networks = {
    "Codethink Guest" = {
      psk = "@GUEST_PASS@";
    };
    "tranquility" = {
      psk = "@TRANQUILITY_PASS@";
    };
    "BTHub5-SHG7" = {
      psk = "@BTHUB5_SHG7_PASS@";
    };
    "iphone" = {
      priority = 11;
      psk = "12345678";
    };
    "BTHub5-23R3" = {
      psk = "@BTHUB5_23R3@";
    };
    "VM3728053" = {
      # authProtocols= ["SAE"];
      auth = ''
        key_mgmt=SAE
        sae_password="@VM3728053@"
        ieee80211w=2
      '';
    };
    "PLUSNET-M63K" = {
      psk = "@PLUSNET_M63K@";
    };
  };
}
