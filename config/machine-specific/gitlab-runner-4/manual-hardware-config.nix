{
  imports = [ # Include the results of the hardware scan.
    ../boards/rpi3.nix
  ];
  networking = {
    hostName = "gitlab-runner-4"; # Define your hostname.
  };
}