{
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };

  boot.loader.raspberryPi = {
    enable = true;
    version = 3;
    firmwareConfig = ''
      core_freq=250
    '';
  };
  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = true;
  networking.interfaces.eth0.useDHCP = true;

  # rpi3 only has 1gb ram so we need a swap file (maybe I should make it bigger )
  swapDevices = [ { device = "/swapfile"; size = 2048; } ];
}