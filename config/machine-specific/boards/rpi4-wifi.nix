{
  # uses sops!
  networking.interfaces.wlan0.useDHCP = true;
  imports = [
      ../../roles/wifi.nix
  ];
}