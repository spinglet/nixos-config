{ pkgs, ... }:
{
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };

# NixOS wants to enable GRUB by default
  boot.loader.grub.enable = false;
# Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_rpi4;
#  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Required for the Wireless firmware
  hardware.enableRedistributableFirmware = true;

  networking.interfaces.eth0.useDHCP = true;

  boot.loader.raspberryPi = {
    enable = true;
    version = 4;
  };

  swapDevices = [ { device = "/swapfile"; size = 2048; } ];
}