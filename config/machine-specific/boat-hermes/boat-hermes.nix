{

  imports = [ # Include the results of the hardware scan.
    ../boards/rpi4.nix
    ../boards/rpi4-wifi.nix
  ];
  networking.hostName = "boat-hermes";
  #  virtualisation.docker.enable = true;
  #  networking.interfaces.eth0.useDHCP = true;
  #  networking.interfaces.wlan0.useDHCP = true;

}
