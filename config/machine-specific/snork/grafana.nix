{
  imports = [
    ../../roles/grafana.nix
  ];
  services.grafana.addr = "${import ../../vars/ips/snork.nix}";
}
