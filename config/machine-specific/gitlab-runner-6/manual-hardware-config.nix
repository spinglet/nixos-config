{
  # this is the 1GB PI compute module
  imports = [ # Include the results of the hardware scan.
    ../boards/rpi4.nix
  ];
  networking = {
    hostName = "gitlab-runner-6"; # Define your hostname.
  };
}