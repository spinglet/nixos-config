{ config, pkgs, ... }:
{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.kernelModules = [ "vfat" "nls_cp437" "nls_iso8859-1" "usbhid" ];
  boot.initrd.luks.yubikeySupport = true;
  boot.initrd.luks.devices = {
    "pv-sean" = {
      device = "/dev/nvme0n1p2";
      preLVM = true;
      yubikey = {
        slot = 1;
        twoFactor = true;
        storage = {
          device = "/dev/nvme0n1p1";
        };
      };
    };
   };

  networking.hostName = "lenovo-laptop"; # Define your hostname.
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;
}