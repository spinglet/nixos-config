{
  services.node-red = {
    enable = true;
    openFirewall = true;
    withNpmAndGcc = true;
  };

  imports = [
    ../../roles/backup-user.nix
  ];
  users.users.datasync = {
     extraGroups = [ "node-red" ];
  };
}