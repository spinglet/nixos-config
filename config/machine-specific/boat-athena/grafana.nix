{
  imports = [
    ../../roles/grafana.nix
  ];
  services.grafana.addr = "${import ../../vars/ips/boat-athena.nix}";
}