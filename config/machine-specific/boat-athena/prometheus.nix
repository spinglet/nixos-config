{ config, pkgs, lib, ... }:
let
  telegrafPort = 9003;
in
{
  networking.firewall.allowedTCPPorts = [ 9090 8088 8086 ];
  services.prometheus = {
     retentionTime = "2y"; # Idealy I'd like to set retaintion by space but i dont see it in the nix config so I'll wait a while todo come back to
    enable = true;
    listenAddress = "100.89.209.13";
    webExternalUrl = "http://100.89.209.13:9090";
    extraFlags = [ ''--web.cors.origin=".*"''];
    exporters = {
      node = {
        enable = true;
        enabledCollectors = [ "systemd" ];
        port = 9002;
      };
    };
    scrapeConfigs = [
      {
        job_name = "chrysalis";
        static_configs = [{
          targets = [ "127.0.0.1:${toString config.services.prometheus.exporters.node.port}" ];
        }];
      }
      {
        job_name = "telegraf";
         static_configs = [{
          targets = [ "127.0.0.1:${toString telegrafPort}" ];
        }];
      }
    ];
  };
  systemd.services.prometheus.after = lib.mkForce [ "tailscale.service" ];
  systemd.services.prometheus.serviceConfig = { RestartSec = 30; };

  services.telegraf = {
      enable = true;

      extraConfig = {
        inputs.mqtt_consumer = {
          servers = [ "tcp://100.96.69.67:1883" ];
          topics = [ "zigbee2mqtt/#" ];
          data_format = "json";
          json_string_fields = [ "water_leak" "contact" "occupancy"];
          qos = 2;
        };

        outputs = {
          prometheus_client = {
            listen = "127.0.0.1:${toString telegrafPort}";
            metric_version = 2;
            export_timestamp = true;
            expiration_interval = "5m";
          };
          influxdb_v2 = {
            urls = ["http://127.0.0.1:8086"];
            ## Organization is the name of the organization you wish to write to.
            organization = "sean";
            ## Destination bucket to write into.
            bucket = "boat";
            ## Token for authentication.
            token = "5LOjvmQfvfdhYvL3-yIWbzmkX-zG0gbrJsSr_kSf3KKHIuh9lcAqkI0RQD5l_uLc_Cd6Fov9e_b-BtSe9t-15A==";
          };
        };
      };
    };

  services.influxdb2.enable = true;
  systemd.services.influxdb2.after = lib.mkForce [ "tailscale.service" ];
}
