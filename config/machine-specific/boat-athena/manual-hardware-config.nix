{
  imports = [
    ../boards/rpi4.nix
  ];
  networking = {
    hostName = "boat-athena"; # Define your hostname.
  };
}