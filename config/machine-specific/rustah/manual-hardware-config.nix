{ config, pkgs, lib, ... }: {
  # this is the 1GB PI compute module
  imports = [ # Include the results of the hardware scan.
    ../boards/rpi3.nix
    ../boards/rpi4-wifi.nix
  ];
  networking = {
    hostName = "rustah"; # Define your hostname.
  };
#  hardware.enableAllFirmware = true;
  hardware.enableRedistributableFirmware = true;

  networking.wireless.networks = lib.mkForce {
      "Codethink Guest" = {
        extraConfig = "bssid_whitelist=7e:8a:20:0a:0e:cb\n";
        psk = "@GUEST_PASS@";
        priority = 10;
      };
  };
}