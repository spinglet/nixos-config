{
  imports = [
    ../boards/rpi4.nix
    ../boards/rpi4-wifi.nix
  ];
  networking = {
    hostName = "gizmo"; # Define your hostname.
  };
}