{ pkgs, lib, config, ... }:
let
  # rclone
    rcloneConficFilePath = "rcloneConfig.conf";
    rclonePubSsh = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBBYnqAOBc3i4DENGunWPILJAPxZjTi81WcKIZgZx0s4 sean@lenovo-laptop";
    # device specific
      rcloneBoatAthenaConfigName = "BoatAthenaSftp";
      rcloneBoatHermesConfigName = "BoatHermesSftp";
      rcloneBackblazeConfigName = "backblazeB2";
    # service specific
      systemdRcloneBoatAthenaNodeRedName = "rcloneNodeRed";
      systemdRcloneBoatAthenaGrafanaName = "rcloneGrafana";
      systemdRcloneBoatHermesZigbee = "rcloneZigbee2mqtt";
      systemdRcloneBackupNas = "rcloneBackupNas";

in
{
  imports = [
    ../../roles/sops.nix
  ];
  sops.secrets.rclone_private_ssh = {
    sopsFile = ./backup-secrets.yaml;
  };
  sops.secrets.gocrypt_password = {
    sopsFile = ./backup-secrets.yaml;
  };
  sops.secrets.backblaze_vars = {
    sopsFile = ./backup-secrets.yaml;
  };

  environment.etc = {
    # Creates /etc/${rcloneConficFilePath}
    ${rcloneConficFilePath} = {
      text = ''
        [${rcloneBoatAthenaConfigName}]
        type = sftp
        host = ${import ../../vars/ips/boat-athena.nix}
        user = datasync
        key_file = ${ toString config.sops.secrets.rclone_private_ssh.path }

        [${rcloneBoatHermesConfigName}]
        type = sftp
        host = ${import ../../vars/ips/boat-hermes.nix}
        user = datasync
        key_file = ${ toString config.sops.secrets.rclone_private_ssh.path }
      '';

      # The UNIX file mode bits
      mode = "0444";
    };
  };


  systemd.services."${systemdRcloneBoatAthenaNodeRedName}" = {
    description = "back up node red";
    serviceConfig = {
      ExecStart = "${pkgs.rclone}/bin/rclone --log-level INFO --config=/etc/${rcloneConficFilePath} sync  ${rcloneBoatAthenaConfigName}:/var/lib/node-red /data/backed-up/BoatActionPi/node-red/";
    };
  };

  systemd.timers."${systemdRcloneBoatAthenaNodeRedName}" = {
    wantedBy = [ "multi-user.target" ];
    timerConfig = {
      OnCalendar = "*-*-* 02:30:00"; # daily
      Unit = "${systemdRcloneBoatAthenaNodeRedName}.service";
    };
  };

  systemd.services."${systemdRcloneBoatAthenaGrafanaName}" = {
    description = "back up Grafana";
    serviceConfig = {
      ExecStart = "${pkgs.rclone}/bin/rclone --log-level INFO --config=/etc/${rcloneConficFilePath} sync  ${rcloneBoatAthenaConfigName}:/var/lib/grafana /data/backed-up/BoatActionPi/grafana/";
    };
  };

  systemd.timers."${systemdRcloneBoatAthenaGrafanaName}" = {
    wantedBy = [ "multi-user.target" ];
    timerConfig = {
      OnCalendar = "*-*-* 02:45:00"; # daily
      Unit = "${systemdRcloneBoatAthenaGrafanaName}.service";
    };
  };

  systemd.services."${systemdRcloneBoatHermesZigbee}" = {
    description = "back up Zigbee";
    serviceConfig = {
      # ignore checksum and size due to the file constantly being writen to resulting in the chekcsum/size check after completeion being incorect everytime
      ExecStart = "${pkgs.rclone}/bin/rclone --log-level INFO  --ignore-size --ignore-checksum --config=/etc/${rcloneConficFilePath} sync  ${rcloneBoatHermesConfigName}:/root/zigbee /data/backed-up/BoatListener/zigbee/";
    };
  };

  systemd.timers."${systemdRcloneBoatHermesZigbee}" = {
    wantedBy = [ "multi-user.target" ];
    timerConfig = {
      OnCalendar = "*-*-* 02:00:00"; # daily
      Unit = "${systemdRcloneBoatHermesZigbee}.service";
    };
  };

  systemd.services."gocryptfs-backup-mount" = {
    description = "gocryptfs mount enc-data";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStartPre = [
        "${pkgs.coreutils}/bin/mkdir -m 0500 -pv /enc-data/backed-up/"
        "${pkgs.e2fsprogs}/bin/chattr +i /enc-data/backed-up/"  # Stop files being accidentally written to unmounted directory
      ];
      ExecStart = "${pkgs.gocryptfs}/bin/gocryptfs -reverse  /data/backed-up /enc-data/backed-up/ -fg  -passfile ${ toString config.sops.secrets.gocrypt_password.path} -allow_other";
      ExecStopPost = "-${pkgs.fuse}/bin/fusermount -u /enc-data/backed-up/";
      KillMode = "process";
      Restart = "on-failure";
    };
  };

  # account key and id I put in an env file using the enviroment veriables setting https://rclone.org/docs/#config-file
  # instead of putting in config file due to config file needing them in plain text,
  # as a result I assumed we dont need to specify the config file in the command aswell, however rclone complains if i dont
  # as it cant write to the nix store so I just set the config anyway
  # fast-list reduces the number of transactions for the trade off of more RAM usage
  # bwlimit limits bandwidth between waking hours to not annoy me while browsing internet
  systemd.services."${systemdRcloneBackupNas}" = {
    description = "back up Nas to backblaze";
    serviceConfig = {
      EnvironmentFile = "${ toString config.sops.secrets.backblaze_vars.path}";
      ExecStart = ''
          ${pkgs.rclone}/bin/rclone --fast-list --bwlimit '06:30,512k 00:00,off' --config=/etc/${rcloneConficFilePath} --log-level INFO sync --links /enc-data/backed-up/ backblaze:/mounted-bucket
        '';
    };
  };

  systemd.timers."${systemdRcloneBackupNas}" = {
    wantedBy = [ "multi-user.target" ];
    timerConfig = {
      OnCalendar = "Mon *-*-* 00:00:00"; # weekly
      Unit = "${systemdRcloneBackupNas}.service";
    };
  };

}