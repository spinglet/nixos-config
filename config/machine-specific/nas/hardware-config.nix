{ config, lib, pkgs, modulesPath, ... }:
{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix")];

  boot.initrd.availableKernelModules = [ "uhci_hcd" "ehci_pci" "ata_piix" "ahci" "xhci_pci" "firewire_ohci" "sd_mod" ];
  boot.initrd.kernelModules = [ "dm-snapshot" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];


  fileSystems."/" = {
    device = "/dev/disk/by-uuid/58d2fb09-d598-486d-a233-f134234e47eb";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/0e6940a7-06ca-4ff5-8e18-a07416c08e60";
    fsType = "ext4";
  };

  fileSystems."/data/backed-up" = {
    device = "my-data-backup-pool";
    fsType = "zfs";
  };

  fileSystems."/data/media" = {
    device = "first-stripped-pool";
    fsType = "zfs";
  };

  swapDevices = [ ];

  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
