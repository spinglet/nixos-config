{ pkgs, ... }:
{
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/disk/by-id/ata-Samsung_SSD_860_EVO_250GB_S4CJNJ0N316990E"; # or "nodev" for efi only
#  boot.loader.grub.useOSProber = true;

  networking = {
    hostName = "nas"; # Define your hostname.
  };

  networking.useDHCP = false;
  networking.interfaces.enp5s0.useDHCP = true;

  environment.systemPackages = [ pkgs.zfs pkgs.rclone ];
  boot.supportedFilesystems = [ "zfs" ];
  networking.hostId = "9fff5310";
  services.zfs.autoScrub.enable = true;
  services.zfs.autoSnapshot.enable = true;

  #  helpful zfs commands
  # zpool import # lists pools not currently available on device
  # pool import -f <pool name> # mount pool
  # zfs set mountpoint=legacy <pool name> # set to legacy mode for nixos
  # zfs set com.sun:auto-snapshot=true my-data-backup-pool
  # zfs list -t snapshot # list snapshots
  # zfs list -ro space <pool-name-optional> # show space on disk aswell as snapshot space
  networking.firewall.allowedTCPPorts = [ 8200 80];
  # minidlna service
  services.minidlna.enable = true;
  services.minidlna.announceInterval = 60;
  services.minidlna.friendlyName = "Media";
  services.minidlna.mediaDirs = [ "V,/data/media/Media/"];



  # security.acme.acceptTerms = true;
  services.nginx = {
  enable = true;
  virtualHosts."_" = {
  default = true;
  extraConfig = "autoindex on;";
  # enableACME = true;
  # forceSSL = true;
  root = "/data/media/Media/";
    listen = [
       {
         addr = "100.108.38.15";
         port = 80;
       }
     ];
    };
  };
  # Optional: You can configure the email address used with Let's Encrypt.
  # This way you get renewal reminders (automated by NixOS) as well as expiration emails.
  #  security.acme.certs = {
  #    "100.108.38.15".email = "nas-lets-encrypt@seanborg.tech";
  #  };
  users.users.youtube-ci = {
    isNormalUser = true;
#    home = "/home/alice";
    description = "youtube fetcher";
#    extraGroups = [ "wheel" "networkmanager" ];
    uid = 1003;
    openssh.authorizedKeys.keys = [
      # CI ssh key
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCyxsiI7j/tHY9lerD7QIkn9r0W80WtUHHb69NQpKEvhH2zTjNLANflmQtvMROoN6SpHnFndpW9B4jLTUUa34BIWpoRPnJ5iht7ljrHW8vyIfu/l9+uz1BJeX5yvjr0hOzlxOFGs6L/Zfm/m01PzGHD2DtvvV0Kgr7cXHrgP374ALb8mZcm7y1QHb0CeUuIwUruc2i4MlWTEqQWW4hMRMCCu1WNGXYZ2/IrcWMnZ96sYiiF6pp3BY/EbQVLRlWvnGZFk307gu91pC/V4nK9dBoSfYo8yCEAHmwsXK7yLA5g6b4NIrncraV0s9SYdFelTbNhr4yV/7I13NQnCX8BouhsL9lxCadPmcOukiW05LvcXF/f2VnrAFc+oLMdnvUzKZDlc364ssUBWyIOku4/d/xGimc8yGFOcwZhwp7kc4RSF03DoiJ0kcWSRMyoaVdytSI2D/mU/nao+b3z2RZSZ7RMBhn0+tEAyXyFkVx6Yqi+Y2cqBkxhGyo8YwKAaLdCoC0= sean@pc"
      # laptop key for debug
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCswlyW1qQ6QCdkchTualqdLgsjaR177Ud9f0+z++dkOnfRQLljcCljh1fdR1pGahBlqzonF4zMXAF873FG/d/H/TwRQQyWtnb4AriddOdV3jJnoKafvHDUxBb40LtvRM6c8d4klyMgw2+EK223OTsQifh8NIuZkqJqyhFuLX5Gtbz96EmXWmbiQsUBL5f131etKZCfFSyX3HbGTeUSCg4on//QXUQER60CW9jr7xeSrnwTuWls2l19IkWQS08/7/GKqRKriLkpgcHIDBpRYc7MUyaZCzEB/3DJ/l/fG8BbtzMCXaHQPMaAOGRI9eQIeWTiyAQf7Pb3RoGudp780845xCLvlcusv+ehR/v6btMssDSBJVkc2axLZdQN8n2muz+tcvVxWA/BUiWpB16/IHY3bBx7v9zvDilEq1orCQDIrq6WlpgssX5oX61dCIDfRi+3amZ1km2fm89NBV39so24wmr8LUlwf/397w+yAnpHr23qCWgT3fMNIvtAhdDjV2M= sean@lenovo-laptop"
      # ios devices
      "sh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO4d4HHRMSJ9A8Lfto3jbfzSRq8DtsKOOJcR2XvnAWGG Shortcuts on Sean’s iPhone"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBWSW0HrNt/0QEql5ndCCCvVCKaezVpyBOphrOwoMMzB Shortcuts on iPad"
    ];
  };

}