{
  # this is the 4GB PI compute module
  imports = [ # Include the results of the hardware scan.
    ../boards/rpi4.nix
  ];
  networking = {
    hostName = "gitlab-runner-5"; # Define your hostname.
  };
}