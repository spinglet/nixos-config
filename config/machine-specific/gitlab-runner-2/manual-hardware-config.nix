{
  imports = [ # Include the results of the hardware scan.
    ../boards/rpi3.nix
  ];
  networking = {
    hostName = "gitlab-runner-2"; # Define your hostname.
  };
}