#!/usr/bin/env perl
use strict;
use warnings FATAL => 'all';
use List::Util qw(first);




my $currentWorkspace = `i3-msg -t get_workspaces |  jq -r ".[] | select(.focused==true).name"`;
my $currentOutput = `i3-msg -t get_workspaces |  jq ".[] | select(.focused==true).output"`;
my @curOutputWorkspaceNames = `i3-msg -t get_workspaces |  jq -r '.[] | select(.output==$currentOutput).name' | grep -v hidden`;



#if ($curOutputWorkspaceNames[0] =~ /$currentWorkspace/)
#{
#    `i3-msg workspace $curOutputWorkspaceNames[-1]`;
#}else
#{
	my $idx = first { $curOutputWorkspaceNames[$_] eq $currentWorkspace } 0..$#curOutputWorkspaceNames;
  `i3-msg workspace $curOutputWorkspaceNames[int($idx) - 1]`;
#}
