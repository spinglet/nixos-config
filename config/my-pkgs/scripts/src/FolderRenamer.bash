for SRC in `ls ./`
do
    DST=`dirname "${SRC}"`/`basename "${SRC}" | sed -r 's/(^)(\w)/\U\2/g'`
    if test -d $SRC
    then
      echo $SRC
    
    if [ "${SRC}" != "${DST}" ]
    then
        [ ! -e "${DST}" ] && mv -T "${SRC}" "${DST}" || echo "${SRC} was not renamed"
    fi
    fi
done