{ pkgs, ... }:
with pkgs; {
  required-scripts = callPackage ./required-scripts { inherit pkgs; };
  scripts = callPackage ./scripts { inherit pkgs; };
}
