#!/usr/bin/env perl
use strict;
use warnings FATAL => 'all';

my ($copyRoot) = @ARGV;

if (not defined $copyRoot) {
  die "need to kow nixos root to copy to\n";
}

my $checkGitHash = `cd $copyRoot/nixpkgs/; git rev-parse HEAD`;
my $systemGitHash = `cd /var/src/nixpkgs/; git rev-parse HEAD`;

if ( $checkGitHash eq $systemGitHash ){
        `rm -rf $copyRoot/nixpkgs/`;
        `cp -r /var/src/nixpkgs/ $copyRoot/nixpkgs/`;
}else{
        print("installed system hash: $systemGitHash\nCommitted system hash: $checkGitHash\n");
        exit 1;
}
