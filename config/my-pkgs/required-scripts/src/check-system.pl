#!/usr/bin/env perl
use strict;
use warnings FATAL => 'all';

my ($configRoot) = @ARGV;

if (not defined $configRoot) {
  die "need to kow nixos root\n";
}

my $currentSystem = `readlink -f /nix/var/nix/profiles/system`;
my $config = `nix show-derivation \$(nix-instantiate  '<nixpkgs/nixos>'  -A system -I $configRoot 2> /dev/null)  | jq -r .[].outputs.out.path`;

if ( $currentSystem eq $config ){
        exit 0;
}else{
        print("installed system: $currentSystem\nCommited system: $config\n");
        exit 1;
}
