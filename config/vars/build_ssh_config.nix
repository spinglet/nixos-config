builtins.replaceStrings ["\n Host"] ["\nHost"]
(
	builtins.toString
	(
		map (
		x: if (builtins.readDir ./ips)."${x}" == "regular"
			then ''
      Host Root-${builtins.substring 0 ((builtins.stringLength x) - 4) x}
        Hostname  ${builtins.import (./ips + builtins.toString("/" + x))}
        User root
        Port 22
        IdentityFile /etc/yubikey.pub
      ''
      else
      ""
		)
		(
			builtins.attrNames  (builtins.readDir ./ips)
		)
	)
)
