{ pkgs, ... }:
let
  popPubSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYGaXoLb72Kb7KJZdPBYhJM9qG41TeObJKgiSDwYu934ErMUE6+d5vEu1YDEsxHsBrTobUBUlsfEVKSgJ6XZqN5HJheNBUNJ9EZd0CNo1MPAUT9a+YeFV9XJoHo97FfS3cAHM33jwwE6qsMXPgTL1sEqPe/swJozt9j+Ot8OnFEy7V6yvrOPJN7b+riu5H4u4WrPGv23rloQur8Zp9ZHqMjBNKS0EIcLUIfHLhcKDMgKBx03PHPAlshRUNpUHzzIKv4mXdVeXpnz2cxQIC5MMxOE3PRU1UySbD4nw5l8tp82B+EVlnf8idx/N9662VJ6s4NMH4GL9bXEVfgc0NOMgn poppy@ct-1024";
  laptopSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD99pJrW69xKMF4g7gmCPCSze7Ic8diTedV43UOlmhub+ogogrQXmfR3I+7hH7kolgpRRsbLSPXXjMXWoeLL2Pte9R3QcUdft1aGMyzpOFRlARIW3IiaJ1okok23RoCnVYJdFiNneunTw5puqf8yQbg+WJPvnChlKN+JcKP737GN80RvhIdnwr98uO3/9vZr5lQ8U2vzhMGth++yC6c68yhYIuwFBuAFMfF+vQz/hdQrIrr9Cx9tBdgJDZZMusgazxUfVprdaF1Zx5Yua5sXYiLE+92UeFajz1tM8Sg5nSu2BivawCiG9/X3rr8VMgXG61A0HJtcUrxAiGF9Cke7cjl poppy@ct-1024";
in
{
  imports = [
    ../imports.nix # this is pulling from the machine after krops has already coppied over the file
  ];

  security.sudo.enable = true;
  security.sudo.extraConfig = ''
    krops ALL=(ALL) /run/current-system/sw/bin/nixos-rebuild -I /var/src switch
    krops ALL=(ALL) /run/current-system/sw/bin/nixos-rebuild -I /var/src switch --no-build-nix
  '';

  users.users.krops = { # remember krops needs to have access to /var/src
    isNormalUser = true;
    home = "/home/krops";
    description = "krops";
    openssh.authorizedKeys.keys = [
      "${popPubSSHKey}"
      "${laptopSSHKey}"
    ];
  };

  time.timeZone = "Europe/London";

  services.tailscale.enable = true;
  services.openssh.enable = true;
  services.openssh.passwordAuthentication = false;
  nixpkgs.overlays = [ (self: super: { local = import ./my-pkgs { pkgs = super; };})];
  environment.systemPackages = [ pkgs.git pkgs.vim pkgs.tailscale pkgs.tree pkgs.local.required-scripts];
  users.users.root.openssh.authorizedKeys.keys = [
    "${popPubSSHKey}"
    "${laptopSSHKey}"
  ];
}
