{
  imports = [ # Include the results of the hardware scan.
    ../config/machine-specific/boat-athena/manual-hardware-config.nix
    ../config/machine-specific/boat-athena/node-red.nix
    ../config/machine-specific/boat-athena/grafana.nix
    ../config/machine-specific/boat-athena/prometheus.nix
  ];
}
