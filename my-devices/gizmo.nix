{
  imports = [ # Include the results of the hardware scan.
    ../config/machine-specific/gizmo/manual-hardware-config.nix
    ../config/machine-specific/gizmo/node-red.nix
    ../config/machine-specific/gizmo/grafana.nix
    ../config/roles/zigbee2mqtt.nix
  ];
}
