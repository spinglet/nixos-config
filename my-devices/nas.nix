{
  imports = [
    ../config/machine-specific/nas/hardware-config.nix
    ../config/machine-specific/nas/manual-hardware-config.nix
    ../config/machine-specific/nas/backup.nix
  ];
}
