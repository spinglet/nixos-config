{
  imports = [ # Include the results of the hardware scan.
    ../config/machine-specific/snork/manual-hardware-config.nix
    ../config/machine-specific/snork/node-red.nix
    ../config/machine-specific/snork/prometheus.nix
    ../config/machine-specific/snork/grafana.nix
    ../config/roles/zigbee2mqtt.nix
  ];
}
