{

imports = [ # Include the results of the hardware scan.
    ../config/machine-specific/lenovo-laptop/manual-hardware-config.nix
    ../config/machine-specific/lenovo-laptop/hardware-configuration.nix
    ../config/roles/laptop.nix
    ../config/roles/development/development.nix
    ../config/roles/personal.nix
    ../config/roles/wifi.nix
    ../config/roles/backblaze-mount.nix

  ];
}
