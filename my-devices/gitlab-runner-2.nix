{
  imports = [ # Include the results of the hardware scan.
    ../config/machine-specific/gitlab-runner-2/manual-hardware-config.nix
    ../config/roles/gitlab-runner.nix
  ];
}
