{
  imports = [ # Include the results of the hardware scan.
    ../config/machine-specific/boat-hermes/boat-hermes.nix
    ../config/roles/zigbee2mqtt.nix
  ];
}
