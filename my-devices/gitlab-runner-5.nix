{
  imports = [ # Include the results of the hardware scan.
    ../config/machine-specific/gitlab-runner-5/manual-hardware-config.nix
    ../config/roles/gitlab-runner.nix
  ];
}
